import configparser
import subprocess
import os
import shutil
import errno
import fileinput
import psutil
import time

config = configparser.ConfigParser()
config.read('config/sites.ini')

#ROOT = "/tmp" # for testing
ROOT = "" 

def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise

def checkIfProcessRunning(processName):
    '''
    Check if there is any running process that contains the given name processName.
    '''
    #Iterate over the all the running process
    for proc in psutil.process_iter():
        try:
            # Check if process name contains the given name string.
            if processName.lower() in proc.name().lower():
                return True
        except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
            pass
    return False;

def process_name(git, orgname, ext):
    name = orgname + ext

    path = git
    path = path.replace(".", "_")
    path = path.replace("_git", "")
    path = path.split('/')[-1]
    path = path + ext

    fullpath = ROOT + "/opt/sites/" + path
    destconf = ROOT + "/etc/apache2/sites-enabled/100-" + name + ".conf"
    destsslconf = ROOT + "/etc/apache2/sites-enabled/100-" + name + "-le-ssl.conf"

    mkdir_p(ROOT + "/etc/apache2/sites-enabled")
    
    if not os.path.isfile(destconf) or not os.path.isfile(destsslconf) or not os.path.exists(fullpath):
        mkdir_p(fullpath)
        
        print "Creating new config " + destconf 
        shutil.copyfile("templates/apache.conf", destconf)
        
        # Read in the file
        with open(destconf, 'r') as file :
          filedata = file.read()
        
        # Replace the target string
        filedata = filedata.replace("NAME", name)
        filedata = filedata.replace("PATH", path)

        # Write the file out again
        with open(destconf, 'w') as file:
          file.write(filedata)

        while checkIfProcessRunning("letsencrypt"):
            print "Waiting for another letsencrypt process"
            time.sleep(10)

        print "Running letsencrypt run -n --apache -d " + name + ".tzm.community"
        print subprocess.check_output(["letsencrypt", "run", "-n", "--apache", "-d", name + ".tzm.community", "--redirect"])

for name in config.sections():
    git = config[name]['git']
    process_name(git, name, "")
    extensions = ['test', 'dev', 'update']
    for e in extensions:
        process_name(git, name, "-" + e)
        
